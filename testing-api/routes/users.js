var express = require('express');
var router = express.Router();
const { ObjectId } = require('mongodb');
const mongoose = require('mongoose');
// Define user schema
const usersSchema = new mongoose.Schema({
  name: String,
  age: Number,
  address: String,
  gender: String,
  section: String,
  keluhan: String,
  antrean: String,
  status: String,
  date_of_birth: Date,
  id: Number,
  created_at: String
});

usersSchema.set('timestamps',
  {
    createdAt: 'crdAt',
  }
)

// Create User model
const Users = mongoose.model('Users', usersSchema);
router.get('/', function (req, res, next) {
  if (req.query.status && req.query.status != null) {
    Users.find({ status: req.query.status })
      .then(function (users) {
        res.status(200).send(users);
      });
  } else {

    Users.find({})
      .then(function (users) {
        res.status(200).send(users);
      });
  }

});


router.post('/', async (req, res) => {

  // #calculate users today 
  let usersToday = []
  const now = new Date();
  const today = new Date(now.getFullYear(), now.getMonth(), now.getDate());
  await Users.find({ crdAt: { $gte: today } }).then((users) => {
    usersToday = users
  });

  // Get Antrean 
  const antrean = usersToday.length

  // Get First Status 
  const status = 'waiting'




  const users = new Users({ ...req.body, antrean, status });
  users.save()
    .then((user) => res.send(user))
    .catch(err => res.status(500).send(err));
})

router.get('/:id', (req, res) => {
  Users.findOne({ "_id": ObjectId(req.params['id']) })
    .then(function (users) {
      res.status(200).send(users)
    })

});

router.put('/:id', (req, res) => {
  const { id } = req.params;
  Users.findByIdAndUpdate(id, { ...req.body }, { new: true })
    .then(user => {
      if (!user) {
        res.status(404).send('User not found');
        return;
      }
      res.json(user);
    })
    .catch(err => res.status(500).send(err));
});

// Menghapus user berdasarkan ID
router.delete('/:id', (req, res) => {
  const { id } = req.params;

  Users.findByIdAndDelete(id)
    .then(user => {
      if (!user) {
        res.status(404).send('User not found');
      }
      else res.status(200).send('User deleted successfully');
    })
    .catch(err => res.status(500).send(err));
});

module.exports = router;
